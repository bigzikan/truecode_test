﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueCode_Test.Tests.Common;
using Xunit;

namespace TrueCode_Test.Tests.Posts.Commands
{
    public class GetPostsByUsersListCommandTests : TestCommandBase
    {
        [Fact]
        public async Task GetPostsByUserIdCommand_IsWrongId()
        {
            // Arrange            

            // Act                        
            var result = await postService.GetPostsByUsersList(-1, 2);

            // Assert
            Assert.True(result.Count == 0);
        }

        [Fact]
        public async Task GetPostsByUserIdCommand_Success()
        {
            // Arrange            

            // Act                        

            // Assert
            Assert.NotNull(await postService.GetPostsByUsersList(4, 2));
        }
    }
}
