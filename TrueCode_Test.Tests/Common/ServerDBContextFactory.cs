﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueCode_Test.Data;

namespace TrueCode_Test.Tests.Common
{
    public class ServerDBContextFactory
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
            return config;
        }

        public static TrueCodeContext Create()
        {
            var config = InitConfiguration();
            var options = new DbContextOptionsBuilder<TrueCodeContext>()
                            .UseNpgsql(config.GetConnectionString("TestConnection"))
                            .Options;

            var context = new TrueCodeContext(options);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            context.Database.EnsureCreated();
            DBObjects.Initial(context);
            //context.Database.Migrate();
            return context;
        }

        public static void Destroy(TrueCodeContext context)
        {
            //context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
