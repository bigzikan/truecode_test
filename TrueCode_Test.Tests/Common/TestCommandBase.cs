﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueCode_Test.Data;
using TrueCode_Test.Data.Repository;
using TrueCode_Test.Services;

namespace TrueCode_Test.Tests.Common
{
    public abstract class TestCommandBase : IDisposable
    {
        protected readonly TrueCodeContext context;
        protected readonly UserRepository userRepository;
        protected readonly PostRepository postRepository;        

        protected readonly UserService userService;
        protected readonly PostService postService;

        public TestCommandBase()
        {
            context = ServerDBContextFactory.Create();

            userRepository = new UserRepository(context);
            postRepository = new PostRepository(context);

            userService = new UserService(userRepository);
            postService = new PostService(postRepository, userRepository);
        }

        public void Dispose()
        {
            ServerDBContextFactory.Destroy(context);
        }
    }
}
