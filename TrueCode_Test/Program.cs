using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System;
using TrueCode_Test.Data;
using TrueCode_Test.Data.Interfaces;
using TrueCode_Test.Data.Repository;
using TrueCode_Test.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddScoped<IPostRepository, PostRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

builder.Services.AddScoped<IPostService, PostService>();
builder.Services.AddScoped<IUserService, UserService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

ConfigurationManager configuration = builder.Configuration;

builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "TrueCode_Test",
        Version = "v1",
        Description = "�������� ������� ��� �������� TrueCode"
    });

    var xmlFile = $"{typeof(Program).Assembly.GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    s.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
});

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddHealthChecks();

builder.Services.AddEntityFrameworkNpgsql().AddDbContext<TrueCodeContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection String 'TrueCodeContext' not found."));
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    string swaggerJsonBasePath = string.IsNullOrWhiteSpace(options.RoutePrefix) ? "." : "..";
    options.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "API");
    options.EnableFilter();
    options.DisplayRequestDuration();
});

using (var scope = app.Services.CreateScope())
{
    TrueCodeContext context = scope.ServiceProvider.GetRequiredService<TrueCodeContext>();
    DBObjects.Initial(context);
};

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UsePathBase(new PathString(configuration["AppName"]));
app.UseAuthorization();

app.MapControllers();
app.MapHealthChecks("/healthz");

app.Run();
