﻿namespace TrueCode_Test.Data.Models
{
    public class User
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ник пользователя
        /// </summary>
        public string NickName { get; set; }
    }
}
