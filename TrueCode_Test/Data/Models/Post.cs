﻿namespace TrueCode_Test.Data.Models
{
    public class Post
    {
        /// <summary>
        /// Id поста
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Дата публикации
        /// </summary>
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// Содержимое поста
        /// </summary>
        public string Content { get; set; }
    }
}
