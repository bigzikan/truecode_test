﻿using Microsoft.EntityFrameworkCore;
using System;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Mocks
{
    public class MockUsers
    {
        private readonly TrueCodeContext _context;

        public MockUsers(TrueCodeContext context)
        {
            _context = context;
        }

        public async Task<List<User>> SetMocks()
        {
            var users = new List<User>();

            for (int i = 0; i < 2; i++)
            {
                var user = new User { NickName = $"Test{i}" };
                await _context.Users.AddAsync(user);
                _context.SaveChanges();
                users.Add(user);
            }

            return users;
        }
    }
}
