﻿using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Mocks
{
    public class MockPosts
    {
        private readonly TrueCodeContext _context;
        private MockUsers _usersMocks;

        public MockPosts(TrueCodeContext context)
        {
            _context = context;
            _usersMocks = new MockUsers(context);
        }

        public async Task SetMocks()
        {
            var users = await _usersMocks.SetMocks();

            foreach (var user in users)
            {
                for (var i = 0; i < 5; i++)
                {
                    var post = new Post
                    {
                        UserId = user.Id,
                        PublicationDate = DateTime.Now,
                        Content = $"User - {user.NickName}, Post{i}"
                    };
                    await _context.Posts.AddAsync(post);
                    _context.SaveChanges();
                }
            }
        }
    }
}
