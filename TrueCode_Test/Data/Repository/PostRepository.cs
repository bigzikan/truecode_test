﻿using Microsoft.EntityFrameworkCore;
using TrueCode_Test.Data.Interfaces;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly TrueCodeContext _appContext;

        public PostRepository(TrueCodeContext appContext)
        {
            _appContext = appContext;
        }

        /// <summary>
        /// Получить N (максимум) последних сообщения конкретного пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="count">Количество сообщений</param>
        /// <returns>Список постов</returns>
        public async Task<List<Post>> GetPostsByUserId(int userId, int count = 0)
        {
            var posts = new List<Post>();

            if (count > 0)
            {
                posts = await _appContext.Posts
                    .Select(x => x)
                    .Where(p => p.UserId == userId)
                    .Take(count)
                    .OrderByDescending(x => x.PublicationDate)
                    .ToListAsync();
            }
            else
            {
                posts = await _appContext.Posts
                    .Select(x => x)
                    .Where(p => p.UserId == userId)
                    .OrderByDescending(x => x.PublicationDate)
                    .ToListAsync();
            }

            return posts;
        }

        /// <summary>
        /// Возвращает usersCount id юзеров, у которых последних были посты
        /// </summary>
        /// <param name="usersCount"></param>
        /// <returns></returns>
        public async Task<List<int>> GetUsersWithLastsPosts(int usersCount)
        {
            var userIds = new List<int>();

            var list = await _appContext.Posts
                .OrderByDescending(x => x.PublicationDate)
                .ToListAsync();

            var groupList = list.GroupBy(x => x.UserId).Take(usersCount).ToList();

            foreach (var item in groupList)
            {
                userIds.Add(item.First().UserId);
            }

            return userIds;
        }
    }
}
