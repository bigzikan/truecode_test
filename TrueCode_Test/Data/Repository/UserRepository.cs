﻿using Microsoft.EntityFrameworkCore;
using TrueCode_Test.Data.Interfaces;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly TrueCodeContext _appContext;

        public UserRepository(TrueCodeContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<User?> GetUserById(int userId)
        {
            return await _appContext.Users.FirstOrDefaultAsync(x => x.Id == userId);           
        }
    }
}
