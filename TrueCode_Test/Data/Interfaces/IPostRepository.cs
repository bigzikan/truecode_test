﻿using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Interfaces
{
    public interface IPostRepository
    {
        /// <summary>
        /// Получить N (максимум) последних сообщения конкретного пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="count">Количество сообщений</param>
        /// <returns>Список постов</returns>
        Task<List<Post>> GetPostsByUserId(int userId, int count = 0);

        /// <summary>
        /// Возвращает usersCount id юзеров, у которых последних были посты
        /// </summary>
        /// <param name="usersCount"></param>
        /// <returns></returns>
        Task<List<int>> GetUsersWithLastsPosts(int usersCount);
    }
}
