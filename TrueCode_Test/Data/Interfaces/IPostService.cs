﻿using TrueCode_Test.Controllers.ResponseModels;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Interfaces
{
    public interface IPostService
    {
        /// <summary>
        /// Получить N (максимум) последних сообщения конкретного пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="count">Количество сообщений</param>
        /// <returns>Список постов</returns>
        Task<List<Post>> GetPostsByUserId(int userId, int count = 0);

        /// <summary>
        /// Получить N (максимум) последних сообщений для разных пользователей
        /// </summary>
        /// <param name="model">Входные данные</param>
        /// <returns>Список постов с пользователями</returns>
        Task<List<GetPostsByUsersListResponse>> GetPostsByUsersList(int usersCount, int postsCount = 0);
    }
}
