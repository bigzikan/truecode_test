﻿using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Data.Interfaces
{
    public interface IUserRepository
    {
        /// <summary>
        /// Возвращает информацию по пользователю
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<User?> GetUserById(int userId);
    }
}
