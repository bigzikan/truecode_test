﻿using Microsoft.EntityFrameworkCore;
using TrueCode_Test.Data.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace TrueCode_Test.Data
{
    public class TrueCodeContext : DbContext
    {
        public TrueCodeContext(DbContextOptions<TrueCodeContext> options) : base(options)
        {
        }

        /// <summary>
        /// Таблица пользователей
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Таблица постов
        /// </summary>
        public DbSet<Post> Posts { get; set; }
    }
}
