﻿using System;
using TrueCode_Test.Data.Mocks;

namespace TrueCode_Test.Data
{
    public class DBObjects
    {
        public static async void Initial(TrueCodeContext context)
        {
            try
            {
                if (!context.Users.Any() || !context.Posts.Any())
                {
                    var mockPosts = new MockPosts(context);

                    await mockPosts.SetMocks();
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
    }
}
