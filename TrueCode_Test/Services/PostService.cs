﻿using Microsoft.AspNetCore.Mvc;
using TrueCode_Test.Controllers.ResponseModels;
using TrueCode_Test.Data.Interfaces;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Services
{
    public class PostService : IPostService
    {
        private IPostRepository _postRepository;
        private IUserRepository _userRepository;

        public PostService(IPostRepository postRepository,
            IUserRepository userRepository)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Получить N (максимум) последних сообщения конкретного пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="count">Количество сообщений</param>
        /// <returns>Список постов</returns>
        public async Task<List<Post>> GetPostsByUserId(int userId, int count = 0)
        {
            return await _postRepository.GetPostsByUserId(userId, count);
        }

        /// <summary>
        /// Получить N (максимум) последних сообщений для разных пользователей
        /// </summary>
        /// <param name="model">Входные данные</param>
        /// <returns>Список постов с пользователями</returns>
        public async Task<List<GetPostsByUsersListResponse>> GetPostsByUsersList(int usersCount, int postsCount = 0)
        {
            var userIDs = await _postRepository.GetUsersWithLastsPosts(usersCount);
            var result = new List<GetPostsByUsersListResponse>();

            foreach (var userID in userIDs)
            {
                var user = await _userRepository.GetUserById(userID);
                var posts = await _postRepository.GetPostsByUserId(userID, postsCount);
                result.Add(new GetPostsByUsersListResponse
                    {
                        UserInfo = user,
                        Posts = posts
                    }
                );
            }

            return result;
        }
    }
}
