﻿using TrueCode_Test.Data.Interfaces;

namespace TrueCode_Test.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository) 
        {
            _userRepository = userRepository;
        }
    }
}
