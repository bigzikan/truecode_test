﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrueCode_Test.Controllers.ResponseModels;
using TrueCode_Test.Data.Interfaces;
using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private IUserService _userService;
        private IPostService _postService;

        public PostController(
            IUserService userService,
            IPostService postService)
        {
            _userService = userService;
            _postService = postService;
        }

        /// <summary>
        /// Получить N (максимум) последних сообщения конкретного пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="count">Количество сообщений</param>
        /// <returns>Список постов</returns>
        [HttpGet]
        [Route("getPostsByUserId/{userId:int}/{count:int}")]
        public async Task<IActionResult> GetPostsByUserId(int userId, int count = 0)
        {
            var result = new List<Post>();
            result = await _postService.GetPostsByUserId(userId, count);
            return Ok(result);
        }

        /// <summary>
        /// Получить N (максимум) последних сообщений для разных пользователей
        /// </summary>
        /// <param name="usersCount">кол-во пользователей</param>
        /// <param name="postsCount">Количество сообщений</param>
        /// <returns>Список постов</returns>
        [HttpGet]
        [Route("getPostsByUsersList/{usersCount:int}/{count:int}")]
        public async Task<IActionResult> GetPostsByUsersList(int usersCount, int postsCount = 0)
        {
            var result = new List<GetPostsByUsersListResponse>();

            result = await _postService.GetPostsByUsersList(usersCount, postsCount);
            return Ok(result);
        }
    }
}
