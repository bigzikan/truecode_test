﻿using TrueCode_Test.Data.Models;

namespace TrueCode_Test.Controllers.ResponseModels
{
    /// <summary>
    /// Модель ответа на запрос GetPostsByUsersList
    /// </summary>
    public class GetPostsByUsersListResponse
    {
        public GetPostsByUsersListResponse() { }

        /// <summary>
        /// Инфо о пользователе
        /// </summary>
        public User UserInfo { get; set; }

        /// <summary>
        /// Список последних сообщений
        /// </summary>
        public List<Post> Posts { get; set; }
    }
}
