# TrueCode_Test

Тестовое задание для компании TrueCode

## Запуск приложения

Для запуска понадобится установленный на компьютере postgres (приложение разрабатывавлось с 14 версией).
База и её структура создаются из миграций во время запуска приложения. Для подключения к БД используются настройки из appsettings.json в разделе ConnectionStrings,
параметр DefaultConnection. Для тестов - TestConnection. При необходимости их нужно изменить. 

## Тестовое задание
на вакансию Разработчика на C# (Middle/Middle+)
Задания:
1. Написать класс, который принимает в конструкторе Stream. У класса должен быть метод
для считывания байтового сообщения из потока. Каждое сообщение оканчивается на
некоторый уникальный символ конца сообщения. Размер буфера (для чтения) и символ конца
сообщения также должны конфигурироваться для этого класса.
2. Есть часть простого движка ленты постов с пользователями и их постами. У каждого
пользователя есть ID и имя. У каждого поста есть его идентификатор, идентификатор
пользователя (создателя), дата публикации и какое-то текстовое сообщение.
Требуется сделать проект ASP.NET Core следующим HTTP API:
● Получить N (максимум) последних сообщения конкретного пользователя
● Получить N (максимум) последних сообщений для разных пользователей. Т.е. взять
максимум N пользователей, которые оставляли сообщения, и выбрать их последние
сообщения в порядке даты публикации.
Добавить тесты к данному функционалу.
Требования к оформлению:
Выполненное тестовое задание должно быть собрано в архив и включать в себя:
1. Все исходные файлы вместе с проектными файлами.
2. Текстовый файл readme.txt с инструкцией по настройке и конфигурированию приложения
(если необходимо).
Анна